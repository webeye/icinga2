#!/usr/bin/env bash
set -e


base_dir=$(dirname $0)

mkdir -p "$base_dir/download"

if [ ! -f $base_dir/download/icingaweb2.zip ]; then
 wget --no-cookies "https://github.com/Icinga/icingaweb2/archive/v2.3.2.zip" -O $base_dir/download/icingaweb2.zip
fi
if [ ! -f $base_dir/download/director.zip ]; then
 wget --no-cookies "https://github.com/Icinga/icingaweb2-module-director/archive/master.zip" -O $base_dir/download/director.zip
fi

docker build -t icinga2 .

docker tag icinga2 wsrak/icinga2

docker push wsrak/icinga2