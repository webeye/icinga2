# Dockerfile for icinga2 with icingaweb2
# https://github.com/jjethwa/icinga2

FROM debian:jessie

MAINTAINER Vaclav Rak

LABEL version="2.4.10"

ENV DEBIAN_FRONTEND noninteractive
ENV ICINGA2_FEATURE_GRAPHITE false
ENV ICINGA2_FEATURE_GRAPHITE_HOST graphite
ENV ICINGA2_FEATURE_GRAPHITE_PORT 2003
ENV TERM xterm

RUN apt-get -qq update && \
    apt-get -qqy install --no-install-recommends bash sudo procps ca-certificates wget supervisor apache2 pwgen unzip ssmtp mailutils vim php5-curl  mysql-client openssh-client && \
    rm -r /var/lib/apt/lists


RUN wget --quiet -O - https://packages.icinga.org/icinga.key | apt-key add -


RUN echo "deb http://packages.icinga.org/debian icinga-jessie main" >> /etc/apt/sources.list && \
    apt-get -qq update && \
    apt-get -qqy install --no-install-recommends icinga2 icinga2-ido-mysql icinga-web nagios-plugins icingaweb2 icingacli && \
    apt-get clean && \
    rm -r /var/lib/apt/lists


# Temporary hack to get icingaweb2 modules via git
RUN usermod -m -d /home/nagios nagios && mkdir -p /etc/icingaweb2/enabledModules /home/nagios/.ssh
ADD download/icingaweb2.zip /tmp/
#RUN wget --no-cookies "https://github.com/Icinga/icingaweb2/archive/v2.3.2.zip" -O /tmp/icingaweb2.zip
RUN unzip /tmp/icingaweb2.zip "icingaweb2-2.3.2/modules/doc/*" "icingaweb2-2.3.2/modules/monitoring/*" -d "/tmp/icingaweb2" && \
    cp -R /tmp/icingaweb2/icingaweb2-2.3.2/modules/monitoring /etc/icingaweb2/modules/ && \
    cp -R  /tmp/icingaweb2/icingaweb2-2.3.2/modules/doc /etc/icingaweb2/modules/ && \
    rm -rf /tmp/icingaweb2.zip /tmp/icingaweb2 && \
    chown nagios: /home/nagios -R

ADD keys/ /home/nagios/.ssh/

ADD content/ /
RUN chmod u+x /opt/supervisor/icinga2_supervisor /opt/supervisor/apache2_supervisor /opt/run /opt/scripts/* && \
    chmod u+s /bin/ping /bin/ping6 && \
    chmod u+rwx,go-rwx /home/nagios/.ssh &&  \
    chmod u+rw,go+r,go-wx,u-x /home/nagios/.ssh/id_rsa.pub

# Icinga Director
ADD download/director.zip /tmp/
#RUN wget --no-cookies "https://github.com/Icinga/icingaweb2-module-director/archive/master.zip" -O /tmp/director.zip
RUN unzip /tmp/director.zip -d "/tmp/director" && \
    cp -R /tmp/director/icingaweb2-module-director-master/* /etc/icingaweb2/modules/director/ && \
    rm -rf /tmp/director




EXPOSE 80 443 5665

VOLUME ["/etc/icinga2", "/var/lib/icinga2", "/etc/icinga-web", "/etc/icingaweb2" ]

# Initialize and run Supervisor
ENTRYPOINT ["/opt/run"]
