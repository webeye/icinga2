#!/usr/bin/env bash

set -e

echo "$(date) $0"
echo $0 $@

MYSQL_DB=$1
MYSQL_HOST=$2
ICINGAWEB2_PASSWORD=$3
MYSQL_ROOT_PASSWORD=$4
ICINGAADMIN_PASSWORD=$5

echo "Rights on ${MYSQL_DB}_web2"

(
    echo "CREATE DATABASE IF NOT EXISTS ${MYSQL_DB}_web2;"
    echo "GRANT SELECT, INSERT, UPDATE, DELETE, DROP, CREATE VIEW, INDEX, EXECUTE ON ${MYSQL_DB}_web2.* TO '${MYSQL_DB}_web2'@'%' IDENTIFIED BY '${ICINGAWEB2_PASSWORD}';"
    echo "quit"
) |
mysql -h ${MYSQL_HOST} -u root -p${MYSQL_ROOT_PASSWORD}

echo "Schema on ${MYSQL_DB}_web2"
mysql -h ${MYSQL_HOST} -u root -p${MYSQL_ROOT_PASSWORD} -f ${MYSQL_DB}_web2 < /usr/share/icingaweb2/etc/schema/mysql.schema.sql >> /opt/icingaweb2-schema.log 2>&1
echo "Create Icinga Admin on ${MYSQL_DB}_web2"
(
    echo "USE ${MYSQL_DB}_web2;"
    echo "DELETE FROM icingaweb_user WHERE name = 'icingaadmin';"
    echo "INSERT INTO icingaweb_user (name, active, password_hash) VALUES ('icingaadmin', 1, '$1$EzxLOFDr$giVx3bGhVm4lDUAw6srGX1');"
#    echo "INSERT IGNORE INTO icingaweb_user (name, active, password_hash) VALUES ('icingaadmin', 1, '${ICINGAADMIN_PASSWORD}');"
    echo "quit"
) | mysql -h ${MYSQL_HOST} -u root -p${MYSQL_ROOT_PASSWORD}  ${MYSQL_DB}_web2

echo "$(date) $0"
