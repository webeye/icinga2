#!/usr/bin/env bash

set -e

echo "$(date) $0"
echo $0 $@

MYSQL_DB=$1
MYSQL_HOST=$2
ICINGA_WEB_PASSWORD=$3
IDO_PASSWORD=$4
ICINGA_PASSWORD=$5

(
    echo "CREATE DATABASE IF NOT EXISTS ${MYSQL_DB}_web;"
    echo "GRANT SELECT, INSERT, UPDATE, DELETE, DROP, CREATE VIEW, INDEX, EXECUTE ON ${MYSQL_DB}_web.* TO 'icinga_web'@'%' IDENTIFIED BY '${ICINGA_WEB_PASSWORD}';"
    echo "quit"
) |
mysql -h ${MYSQL_HOST} -u root -p${MYSQL_ROOT_PASSWORD}
mysql -h ${MYSQL_HOST} -u root -p${MYSQL_ROOT_PASSWORD} -f ${MYSQL_DB}_web < /usr/share/dbconfig-common/data/icinga-web/install/mysql >> /opt/icinga-web-schema.log 2>&1



sed -i 's/mysql\:\/\/icinga_web\:.*\@localhost/mysql\:\/\/${MYSQL_USER}_web\:'${ICINGA_WEB_PASSWORD}'\@'${MYSQL_HOST}'/g' /etc/icinga-web/conf.d/database-web.xml
sed -i 's/mysql\:\/\/.*\@localhost\/icinga.*</mysql\:\/\/icinga2-ido-mysq:'${IDO_PASSWORD}'\@'${MYSQL_HOST}'\/icinga2idomysql</g' /etc/icinga-web/conf.d/database-ido.xml
sed -i 's,mysql://icinga_web:.*@localhost,mysql://icinga_web:'${ICINGA_WEB_PASSWORD}'@'${MYSQL_HOST}',g' /etc/icinga-web/conf.d/databases.xml
sed -i 's,mysql://icinga:.*@localhost,mysql://icinga:'${ICINGA_PASSWORD}'@'${MYSQL_HOST}',g' /etc/icinga-web/conf.d/databases.xml
echo "$(date) $0"