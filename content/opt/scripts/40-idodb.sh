#!/usr/bin/env bash

set -e
echo $(date) $0
echo $0 $@

MYSQL_HOST=$1
MYSQL_ROOT_PASSWORD=$2
IDO_PASSWORD=$3

sed -i 's,<resource name="icinga_pipe">.*</resource>,<resource name="icinga_pipe">/run/icinga2/cmd/icinga2.cmd</resource>,g' /etc/icinga-web/conf.d/access.xml
sed -i 's/password \= \".*\"/password \= \"'${IDO_PASSWORD}'\"/g' /etc/icinga2/features-available/ido-mysql.conf
sed -i 's/user =\ \".*\"/user =\ \"icinga2-ido-mysq\"/g' /etc/icinga2/features-available/ido-mysql.conf
sed -i 's/database =\ \".*\"/database =\ \"icinga2idomysql\"/g' /etc/icinga2/features-available/ido-mysql.conf

(
    echo "CREATE DATABASE IF NOT EXISTS icinga2idomysql;"
    echo "GRANT SELECT, INSERT, UPDATE, DELETE, DROP, CREATE VIEW, INDEX, EXECUTE ON icinga2idomysql.* TO 'icinga2-ido-mysq'@'%' IDENTIFIED BY '${IDO_PASSWORD}';"
    echo "quit"
) |
mysql -h ${MYSQL_HOST} -u root -p${MYSQL_ROOT_PASSWORD}
mysql -h ${MYSQL_HOST} -u root -p${MYSQL_ROOT_PASSWORD} -f icinga2idomysql < /usr/share/dbconfig-common/data/icinga2-ido-mysql/install/mysql >> /opt/icinga2-ido-mysql-schema.log 2>&1
echo "$(date) $0"