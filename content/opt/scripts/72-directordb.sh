#!/usr/bin/env bash

set -e
echo "$(date) $0"
echo $0 $@

MYSQL_DB=$1
MYSQL_USER=$2
MYSQL_HOST=$3
DIRECTOR_API_PASSWORD=$4
MYSQL_ROOT_PASSWORD=$5
DIRECTOR_PASSWORD=$6


# director
(
    echo "CREATE DATABASE IF NOT EXISTS ${MYSQL_DB}_director CHARACTER SET 'utf8';"
    echo "GRANT ALL ON ${MYSQL_DB}_director.* TO '${MYSQL_USER}_director'@'%' IDENTIFIED BY '${DIRECTOR_PASSWORD}';"
    echo "quit"
) |
mysql -h ${MYSQL_HOST} -u root -p${MYSQL_ROOT_PASSWORD}

mysql -h ${MYSQL_HOST} -u root -p${MYSQL_ROOT_PASSWORD}  -f ${MYSQL_DB}_director < /etc/icingaweb2/modules/director/schema/mysql.sql >> /opt/director-schema.log 2>&1

sed -i 's,director_changeme,'${DIRECTOR_PASSWORD}',g' /etc/icingaweb2/resources.ini
sed -i 's,dbname_director,'${MYSQL_DB}_director',g' /etc/icingaweb2/resources.ini
sed -i 's,username_director,'${MYSQL_USER}_director',g' /etc/icingaweb2/resources.ini

if [[ $(grep director /etc/icinga2/conf.d/api-users.conf) ]]; then echo "=> Director Icinga2 API user exists /etc/icinga2/conf.d/api-users.conf"; else cat /etc/icingaweb2/modules/director/director.api >> /etc/icinga2/conf.d/api-users.conf; fi
sed -i 's,directorapi,'${DIRECTOR_API_PASSWORD}',g' /etc/icinga2/conf.d/api-users.conf
sed -i 's,changeme,'$(hostname)',g' /etc/icingaweb2/modules/director/kickstart.ini
sed -i 's,directorapi,'${DIRECTOR_API_PASSWORD}',g' /etc/icingaweb2/modules/director/kickstart.ini
#sed -i 's,resource_director,'${MYSQL_DB}_director',g' /etc/icingaweb2/modules/director/config.ini
if [[ -L /etc/icingaweb2/enabledModules/director ]]; then echo "Symlink for /etc/icingaweb2/enabledModules/director exists already...skipping"; else ln -sf /etc/icingaweb2/modules/director /etc/icingaweb2/enabledModules/director; fi
echo "$(date) $0"
