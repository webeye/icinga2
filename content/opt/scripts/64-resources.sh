#!/usr/bin/env bash

set -e
echo "$(date) $0"
echo $0 $@

MYSQL_HOST=$1
MYSQL_DB=$2
MYSQL_USER=$3
IDO_PASSWORD=$4
ICINGAWEB2_PASSWORD=$5

if [[ -L /etc/icingaweb2/enabledModules/monitoring ]]; then echo "Symlink for /etc/icingaweb2/enabledModules/monitoring exists already...skipping"; else ln -sf /etc/icingaweb2/modules/monitoring /etc/icingaweb2/enabledModules/monitoring; fi
if [[ -L /etc/icingaweb2/enabledModules/doc ]]; then echo "Symlink for /etc/icingaweb2/enabledModules/doc exists already...skipping"; else ln -sf /etc/icingaweb2/modules/doc /etc/icingaweb2/enabledModules/doc; fi
sed -i 's,localhost,'${MYSQL_HOST}',g' /etc/icingaweb2/resources.ini
sed -i 's,localhost,'${MYSQL_HOST}',g' /etc/icinga2/features-enabled/ido-mysql.conf
sed -i 's,dbname_icingaweb2,'${MYSQL_DB}_web2',g' /etc/icingaweb2/resources.ini
sed -i 's,username_icingaweb2,'${MYSQL_USER}_web2',g' /etc/icingaweb2/resources.ini

sed -i 's,icingaweb2_changeme,'${ICINGAWEB2_PASSWORD}',g' /etc/icingaweb2/resources.ini
sed -i 's,icinga2-ido-mysq_changeme,'${IDO_PASSWORD}',g' /etc/icingaweb2/resources.ini
mkdir -p /var/log/icingaweb2
chown www-data:adm /var/log/icingaweb2

echo "$(date) $0"
