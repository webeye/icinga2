#!/usr/bin/env bash

set -e

echo "$(date) $0"
echo $0 $@

chmod 1777 /tmp

# chown directories and files that might be coming from volumes
chown -R nagios:root /etc/icinga2
chown -f nagios:nagios /etc/icinga2/features-available/ido-mysql.conf
chown -R nagios:nagios /var/lib/icinga2
chown -R root:root /etc/icinga-web
chown -f root:www-data /etc/icinga-web/conf.d/module_reporting.xml /etc/icinga-web/conf.d/auth.xml /etc/icinga-web/conf.d/access.xml /etc/icinga-web/conf.d/database-web.xml /etc/icinga-web/conf.d/databases.xml
chown www-data:icingaweb2 /etc/icingaweb2
chmod 2770 /etc/icingaweb2
chown -R www-data:icingaweb2 /etc/icingaweb2/*
find /etc/icingaweb2 -type f -name "*.ini" -exec chmod 660 {} \;
find /etc/icingaweb2 -type d -exec chmod 2770 {} \;


echo "$(date) $0"