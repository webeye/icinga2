#!/usr/bin/env bash

set -e
echo "$(date) $0"
echo $0 $@

MYSQL_DB=$1
MYSQL_USER=$2
ICINGA_PASSWORD=$3
MYSQL_HOST=$4
MYSQL_ROOT_PASSWORD=$5

echo "=>Initializing databases and icinga2 configurations."
echo "=>This may take a few minutes"


(
    echo "CREATE DATABASE IF NOT EXISTS ${MYSQL_DB};"
    echo "GRANT SELECT, INSERT, UPDATE, DELETE, DROP, CREATE VIEW, INDEX, EXECUTE ON ${MYSQL_DB}.* TO '${MYSQL_USER}'@'%' IDENTIFIED BY '${ICINGA_PASSWORD}';"
    echo "quit"
)|
mysql -h ${MYSQL_HOST} -u root -p${MYSQL_ROOT_PASSWORD}
mysql -h ${MYSQL_HOST} -u root -p${MYSQL_ROOT_PASSWORD} -f  ${MYSQL_DB} < /usr/share/icinga2-ido-mysql/schema/mysql.sql >> /opt/icinga2-ido-mysql-schema.log 2>&1
echo "$(date) $0"