#!/usr/bin/env bash

set -e
echo "$(date) $0"
echo $0 $@


# Graphite writer
if [ "${ICINGA2_FEATURE_GRAPHITE}" == "true" ] || [ "${ICINGA2_FEATURE_GRAPHITE}" == "1" ]; then
   echo "=> Enabling Icinga2 graphite writer"
   if [[ -L /etc/icinga2/features-enabled/graphite.conf ]]; then echo "Symlink for /etc/icinga2/features-enabled/graphite.conf exists already...skipping"; else ln -sf /etc/icinga2/features-available/graphite.conf /etc/icinga2/features-enabled/graphite.conf; fi
cat <<EOF >/etc/icinga2/features-available/graphite.conf
/**
 * The GraphiteWriter type writes check result metrics and
 * performance data to a graphite tcp socket.
 */
library "perfdata"
object GraphiteWriter "graphite" {
  host = "$ICINGA2_FEATURE_GRAPHITE_HOST"
  port = "$ICINGA2_FEATURE_GRAPHITE_PORT"
}
EOF

fi

